﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;
using Typeqast.Domain.Infrastructure.Services;
using Typeqast.Domain.Models;

namespace Typeqast.Test
{
    [TestClass]
    public class AssignmentScenarioTests
    {
        [TestMethod]
        public void SB_CheckPrice_OneBread_OneButter_OneMilk()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 2.95m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_CheckPrice_TwoButters_TwoBreads()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 3.10m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_CheckPrice_FourMilkProducts()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 3.45m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_CheckPrice_TwoButters_OneBread_EightMilkProducts()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 9.00m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
    }
}
