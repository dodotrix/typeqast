using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Typeqast.Domain.Infrastructure;
using Typeqast.Domain.Infrastructure.Services;
using Typeqast.Domain.Models;

namespace Typeqast.Test
{
    [TestClass]
    public class ShoppingBasketTest
    {
        [TestMethod]
        public void SB_CheckPrice_OneProduct()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 0.80m;
            Product Butter = new Product
            {
                Name = "Butter",
                Price = 0.80m
            };

            // act
            sps.Add(Butter);
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_CheckPrice_TwoDifferentProducts()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 1.95m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                }
            };

            // act
            foreach (var p in products) {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_CheckPrice_ThreeDifferenctProducts()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 2.95m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_DiscountOne_CheckPrice_TwoButters_OneBread()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 2.10m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_DiscountOne_CheckPrice_TwoButters_ThreeBread()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 4.10m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }

        [TestMethod]
        public void SB_DiscountOne_CheckPrice_ThreeButters_OneBread()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 2.90m;
            List<Product> products = new List<Product>
            {
                 new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_DiscountOne_CheckPrice_ThreeButters_TwoBread()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 3.90m;
            List<Product> products = new List<Product>
            {
                 new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_DiscountOne_CheckPrice_FourButters_TwoBreads()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 4.20m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                }
            };
            
            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_DiscountOne_CheckPrice_SixButters_ThreeBreads()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 6.30m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                 new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Butter",
                    Price = 0.80m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                },
                new Product
                {
                    Name = "Bread",
                    Price = 1.00m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_DiscountTwo_CheckPrice_TwoMilkProducts()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 2.30m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_DiscountTwo_CheckPrice_ThreeMilkProducts()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 3.45m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }
        [TestMethod]
        public void SB_DiscountTwo_CheckPrice_EightMilkProducts()
        {
            // arrange
            ShoppingBasket sp = new ShoppingBasket();
            ShoppingBasketService sps = new ShoppingBasketService(sp);
            var expected = 6.90m;
            List<Product> products = new List<Product>
            {
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                },
                new Product
                {
                    Name = "Milk",
                    Price = 1.15m
                }
            };

            // act
            foreach (var p in products)
            {
                sps.Add(p);
            };
            var result = sps.ComputePrice(sp);

            // assert
            Assert.AreEqual(expected, result);
        }


    }
}
