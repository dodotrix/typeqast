﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Typeqast.Domain.Models
{
    public class Product
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
