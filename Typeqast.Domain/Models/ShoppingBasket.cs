﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Typeqast.Domain.Models
{
    public class ShoppingBasket
    {
        public List<Product> Products { get; set; } = new List<Product>();
        public decimal TotalPrice { get; set; }

        public override string ToString()
        {
            var json = JsonConvert.SerializeObject(this);
            return json;
        }

    }
}
