﻿using System;
using System.Collections.Generic;
using System.Text;
using Typeqast.Domain.Infrastructure.Interfaces;

namespace Typeqast.Domain.Infrastructure.Services
{
    public class LoggingService : ILoggingService
    {
        public void Log<T>(T data)
        {
            System.Diagnostics.Debug.WriteLine(data.ToString());
        }
    }
}
