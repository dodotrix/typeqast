﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Typeqast.Domain.Infrastructure.Interfaces;
using Typeqast.Domain.Models;

namespace Typeqast.Domain.Infrastructure.Services
{
    public class ShoppingBasketService : IShoppingBasketService
    {
        private readonly ShoppingBasket _sp;
        private readonly DiscountService _discountService;
        private readonly LoggingService _loggingService;
        
        public ShoppingBasketService(ShoppingBasket sp)
        {
            _sp = sp;
            _discountService = new DiscountService();
            _loggingService = new LoggingService();
        }
       
        public void Add(Product product)
        {
            _sp.Products.Add(product);
        }

        public decimal ComputePrice(ShoppingBasket basket)
        {
            decimal nonDiscounted = basket.Products.Sum(x => x.Price);
            decimal discountedValue = _discountService.CalculateAll(basket);
            basket.TotalPrice = nonDiscounted - discountedValue;

            _loggingService.Log<ShoppingBasket>(basket);

            return basket.TotalPrice;
        }
    }
}
