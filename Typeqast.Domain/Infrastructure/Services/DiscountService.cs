﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Typeqast.Domain.Infrastructure.Interfaces;
using Typeqast.Domain.Models;

namespace Typeqast.Domain.Infrastructure.Services
{
    public class DiscountService : IDiscountService
    {
        private readonly LoggingService _loggingService;

        public DiscountService()
        {
            _loggingService = new LoggingService();
        }

        public decimal CalculateAll(ShoppingBasket basket)
        {
            var discount = BuyTwoButtersGetOneBreadHalfPrice(basket);
            discount += BuyThreeMilkGetFourthFree(basket);
            return discount;
        }

        private decimal BuyTwoButtersGetOneBreadHalfPrice(ShoppingBasket basket)
        {
            decimal discount = 0.0m;

            var butters = basket.Products.Where(x => x.Name == "Butter");
            var bread = basket.Products.Where(x => x.Name == "Bread");

            if(bread.Count() > 0 && butters.Count() > 1)
            {
                int j = 1;
                for (int i = 2; i <= butters.Count(); i += 2)
                {
                    if (bread.Count() >= j)
                    {
                        _loggingService.Log<string>("Apply two butter one bread half price discount!");
                        discount += (bread.FirstOrDefault().Price / 2);
                    }
                    j++;
                }
            }

            return discount;
        }

        private decimal BuyThreeMilkGetFourthFree(ShoppingBasket basket)
        {
            decimal discount = 0.0m;

            var milk = basket.Products.Where(x => x.Name == "Milk");

            if (milk.Count() > 3)
            {
                for(int i = 0; i < milk.Count(); i += 4)
                {
                    _loggingService.Log<string>("Apply three milk fourth free discount!");
                    discount += milk.FirstOrDefault().Price;
                }
            }

            return discount;
        }

    }
}
