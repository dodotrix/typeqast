﻿using System;
using System.Collections.Generic;
using System.Text;
using Typeqast.Domain.Models;

namespace Typeqast.Domain.Infrastructure.Interfaces
{
    public interface IDiscountService
    {
        decimal CalculateAll(ShoppingBasket basket);
    }
}
