﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Typeqast.Domain.Infrastructure.Interfaces
{
    public interface ILoggingService
    {
        void Log<T>(T data);
    }
}
